;
(function () {
  function Grid() {
    this.ctx = document.getElementById(arguments[0]).getContext('2d');
    this.audio = document.getElementById(arguments[1]);
    this.socketId = arguments[2].id;
    this.init();
  }

  Grid.prototype.init = function () {
    return this.initParam().drawText().drawGrid();
  }

  Grid.prototype.initParam = function () {
    this.span = 40;
    this.height = this.ctx.canvas.height;
    this.width = this.ctx.canvas.width - 200;
    this.perHeight = this.height / this.span;
    this.perWidth = this.width / this.span;
    return this;
  }

  Grid.prototype.drawText = function () {
    var me = this;
    this.ctx.fillStyle = 'red';
    this.ctx.font = "bold 18px Arial";
    //clearRect(this.width+20,60,200,20);
    //this.ctx.fillText('多人在线贪吃蛇蛇游戏哈哈哈哈哈', this.width + 20, 60);
    this.ctx.fillText('多人在线贪吃蛇蛇游戏', this.width + 10, 80);
    this.ctx.fillText('↑↓←→控制蛇的方向', this.width + 10, 110);
    this.ctx.fillText('空格复活', this.width + 10, 140);
    var img = document.createElement('img');
    img.onload = function () {
      me.ctx.drawImage(this, 0, 0, this.width, this.height, me.width + 10, 153, me.perWidth, me.perHeight);
      me.ctx.fillText('加速蘑菇', me.width + me.perWidth + 15, 170);
    };
    img.src = '/public/img/mushroom.png';
    return this;
  }

  Grid.prototype.showTip = function () {
    var me = this;
    if (!this.tipSettimeoutId) {
      this.tipSettimeoutId = setTimeout(function () {
        me.ctx.fillStyle = 'red';
        me.ctx.clearRect(me.width + 10, 180, 200, 30);
        me.ctx.fillText(me.msg.shift(), me.width + 10, 200);
        if (!me.msg.length) {
          clearTimeout(me.tipSettimeoutId);
          me.tipSettimeoutId = 0;
        }
      }, 1000);
    }
  }

  Grid.prototype.pushMsg = function (msg) {
    (this.msg = (this.msg || [])).push(msg.replace(this.socketId, '您'));
    this.showTip();
    return this;
  }

  Grid.prototype.playAudio = function () {
    if (this.audio && this.audio.play) {
      this.audio.play();
    }
    return this;
  }

  Grid.prototype.drawGrid = function () {
    this.ctx.clearRect(0, 0, this.width, this.height);
    this.ctx.save();
    this.ctx.lineWidth = 2;
    this.ctx.strokeStyle = 'black';
    for (var i = 0; i <= this.span; i++) {
      this.ctx.moveTo(i * this.perWidth, 0);
      this.ctx.lineTo(i * this.perWidth, this.height);
      this.ctx.moveTo(0, i * this.perHeight);
      this.ctx.lineTo(this.width, i * this.perHeight);
    }
    this.ctx.stroke();
    this.ctx.restore();
    return this;
  }

  Grid.prototype.receive = function (grid) {
    var me = this;
    if (grid) {
      me.drawGrid();
      me.ctx.save();
      me.ctx.fillStyle = 'red';
      me.ctx.strokeStyle = 'black';
      grid.foods.forEach(function (food) {
        switch (food.type) {
          case 'food':
            food.coordinates.forEach(function (coordinate) {
              me.ctx.beginPath();
              me.ctx.rect(coordinate.x * me.perWidth, coordinate.y * me.perHeight, me.perWidth, me.perHeight);
              me.ctx.fill();
              me.ctx.stroke();
            });
            break;
          case 'mushroom':
            var img = document.createElement('img');
            img.onload = (function (x, y) {
              return function () {
                me.ctx.drawImage(this, 0, 0, this.width, this.height, x, y, me.perWidth, me.perHeight);
              }
            })(food.coordinates[0].x * me.perWidth, food.coordinates[0].y * me.perHeight);
            img.src = '/public/img/mushroom.png';
            break;
        }
      });
      me.ctx.restore();
      me.ctx.save();
      me.ctx.strokeStyle = 'black';
      grid.snakes.forEach(function (snake) {
        if (!snake.live) {//dead
          me.playAudio();
          me.ctx.fillStyle = 'white';
        } else if (snake.id === me.socketId) {
          me.ctx.fillStyle = 'red';
        } else {
          me.ctx.fillStyle = 'blue';
        }
        if (snake.coordinates[0].y === 49) {
          console.log(snake.coordinates)
        }
        snake.coordinates.forEach(function (coordinate) {
          me.ctx.beginPath();
          me.ctx.rect(coordinate.x * me.perWidth, coordinate.y * me.perHeight, me.perWidth, me.perHeight);
          me.ctx.fill();
          me.ctx.stroke();
        });
      });
      me.ctx.restore();
    }
    return this;
  }

  window.Grid = Grid;
})();


;
$(function () {
  var keys = {'37': 'left', '38': 'up', '39': 'right', '40': 'down', '32': 'revive'};
  var socket = io.connect();
  socket.on('connect', function () {
    var grid = new Grid('canvas', 'bong', socket);
    socket.on('message', function (msg) {
      grid.receive(msg);
    }).on('disconnect', function () {
    }).on('tip', function (msg) {
      grid.pushMsg(msg);
    });
  });
  $(window).on('keydown', function (e) {
    if (keys[e.which]) {
      socket.send(keys[e.which]);
    }
  });
});