var Food = require('./Food');

module.exports = Food.extend({
  init: function () {
    this.callSuper();
    this.type = 'mushroom';
  },
  effect: function (snake) {
    if (snake) {
      snake.speedUp();
    }
    return this;
  }
});