var Class = require('./Class');

module.exports = Class.extend({
  init: function () {
    this.type = 'food';
    this.live = true;
  },
  setCoordinate: function () {
    this.coordinates = arguments[0];
    return this;
  },
  effect: function (snake) {
    return this;
  }
});